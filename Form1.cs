﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrandnameSMS
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private static string GetSampleXml(Sms sms)
		{
			string APIKey = sms.ApiKey;
			string SecretKey = sms.SecretKey;
			string url = sms.Url;
			// declare ascii encoding
			UTF8Encoding encoding = new UTF8Encoding();

			string strResult = string.Empty;

			string SampleXml =
								@"<submission>"
								+ "<api_key>" + APIKey + "</api_key>"
								+ "<api_secret>" + SecretKey + "</api_secret>"
								+ "<sms>"
								+ "<id>" + sms.Id + "</id>"
								+ "<brandname>" + sms.BrandName + "</brandname>"
								+ "<text>" + sms.Text + "</text>"
								+ "<to>" + sms.ReceivePhoneNumber + "</to>"
								+ "</sms>"
								+ "</submission>";

			string postData = SampleXml.Trim();
			byte[] data = encoding.GetBytes(postData);
			WebRequest request = WebRequest.Create(url);
			request.Credentials = CredentialCache.DefaultCredentials;
			request.Method = "POST";
			request.Timeout = 500000;
			request.ContentType = "application/xml";
			request.ContentLength = data.Length;
			Stream newStream = request.GetRequestStream();
			newStream.Write(data, 0, data.Length);
			newStream.Close();

			try
			{
				using (WebResponse response = request.GetResponse())
				{
					// declare & read response from service
					WebResponse webresponse = request.GetResponse();
					Encoding enc = Encoding.GetEncoding("utf-8");
					StreamReader loResponseStream =
						new StreamReader(webresponse.GetResponseStream(), enc);

					strResult = loResponseStream.ReadToEnd();

					loResponseStream.Close();
				}
			}
			catch (WebException e)
			{
				using (WebResponse response = e.Response)
				{
					HttpWebResponse httpResponse = (HttpWebResponse)response;
					using (Stream stream = response.GetResponseStream())
					using (var reader = new StreamReader(stream))
					{
						strResult = reader.ReadToEnd();
					}
				}
			}

			// declare & read response from service

			strResult = strResult.Replace("</string>", "");
			return strResult;
		}

		private static string GetSampleJson(Sms sms)
		{
			string APIKey = sms.ApiKey;
			string SecretKey = sms.SecretKey;
			string url = sms.Url;
			// declare ascii encoding
			UTF8Encoding encoding = new UTF8Encoding();

			string strResult = string.Empty;

			string SampleXml = @"{""submission"":{"
										+ @"""api_key"":""" + APIKey + @""","
										+ @"""api_secret"":""" + SecretKey + @""","
										+ @"""sms"":["
											+ @"{ ""brandname"":""" + sms.BrandName + @""",""text"":""" + sms.Text + @""",""to"":""" + sms.ReceivePhoneNumber + @"""}"
										+ "]"
									+ "}"
								+ "}";

			string postData = SampleXml.Trim();
			byte[] data = encoding.GetBytes(postData);
			WebRequest request = WebRequest.Create(url);
			request.Method = "POST";
			request.Timeout = 500000;
			request.ContentType = "application/json";
			request.ContentLength = data.Length;
			Stream newStream = request.GetRequestStream();
			newStream.Write(data, 0, data.Length);
			newStream.Close();

			try
			{
				using (WebResponse response = request.GetResponse())
				{
					// declare & read response from service
					Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
					StreamReader loResponseStream =
						new StreamReader(response.GetResponseStream(), enc);

					strResult = loResponseStream.ReadToEnd();

					loResponseStream.Close();
				}
			}
			catch (WebException e)
			{
				using (WebResponse response = e.Response)
				{
					HttpWebResponse httpResponse = (HttpWebResponse)response;
					using (Stream stream = response.GetResponseStream())
					using (var reader = new StreamReader(stream))
					{
						// text is the response body
						strResult = reader.ReadToEnd();
					}
				}
			}


			return strResult;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			var smsJson = new Sms
			{
				ApiKey = "1ds4u6by",
				SecretKey = "98h61ku4",
				Url = "https://sms.vht.com.vn/ccsms/json",
				BrandName = "VHT",
				Id = Guid.NewGuid().ToString(),
				Text = "Hi There, Json!",
				ReceivePhoneNumber = "0938882601"
			};
			var responseJson = GetSampleJson(smsJson);
			MessageBox.Show(responseJson);

			//var smsXml = new Sms
			//{
			//    ApiKey = "1ds4u6by",
			//    SecretKey = "98h61ku4",
			//    Url = "https://sms.vht.com.vn/ccsms/xml",
			//    BrandName = "VHT",
			//    Id = Guid.NewGuid().ToString(),
			//    Text = "Hi There, XML!",
			//    ReceivePhoneNumber = "0938882601"
			//};
			//var responseXml = GetSampleXml(smsXml);
			//MessageBox.Show(responseXml);
		}
	}

	public class Sms
	{
		public string ApiKey { get; set; }
		public string SecretKey { get; set; }
		public string Url { get; set; }
		public string Id { get; set; }
		public string BrandName { get; set; }
		public string Text { get; set; }
		public string ReceivePhoneNumber { get; set; }
	}
}
